export class Paciente {
    id: number;
    name: string;
    lastname: string;
    dni: string;
    address: string;
    phone: string;
}