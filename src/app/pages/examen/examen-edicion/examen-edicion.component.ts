import { Component, OnInit } from '@angular/core';
import { Examen } from '../../../_model/examen';
import { FormGroup, FormControl } from '@angular/forms';
import { ExamenService } from '../../../_service/examen.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-examen-edicion',
  templateUrl: './examen-edicion.component.html',
  styleUrls: ['./examen-edicion.component.css']
})
export class ExamenEdicionComponent implements OnInit {

  id: number;
  examen: Examen = new Examen();;
  form: FormGroup;
  edicion: boolean = false;

  constructor(
    private examenService: ExamenService, 
    private route: ActivatedRoute, 
    private router: Router) {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'name': new FormControl(''),
      'description': new FormControl(''),
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      console.log(this.id);
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.examenService.listarExamenPorId(this.id).subscribe(data => {
        this.examen = data;
        this.form = new FormGroup({
          'id': new FormControl(this.examen.id),
          'name': new FormControl(this.examen.name),
          'description': new FormControl(this.examen.description)
        });
      });
    }
  }

  operar() {
    this.examen = this.form.value;

    if (this.examen != null && this.examen.id > 0) {
      this.examenService.modificar(this.examen).subscribe(data => {
        this.examenService.listarExamenes().subscribe(especialidad => {
          this.examenService.examenCambio.next(especialidad);
          this.examenService.mensaje.next("Se modifico");
        });
      });
    } else {
      this.examenService.registrar(this.examen).subscribe(data => {
        this.examenService.listarExamenes().subscribe(especialidad => {
          this.examenService.examenCambio.next(especialidad);
          this.examenService.mensaje.next("Se registro");
        });
      });
    }

    this.examenService.listarExamenes().subscribe(data => {
      this.examenService.examenCambio.next(data);
    });

    this.router.navigate(['examen']);
  }
}
