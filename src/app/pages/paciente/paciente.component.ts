import { Component, OnInit, ViewChild } from '@angular/core';
import { Paciente } from '../../_model/paciente';
import { PacienteService } from '../../_service/paciente.service';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  lista: Paciente[] = [];
  cantidad: number;

  displayedColumns = ['id', 'name', 'lastname', 'acciones'];
  dataSource: MatTableDataSource<Paciente>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private pacienteService: PacienteService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    // Se obtiene el cambio que se está realizando sobre la variable reactiva
    // Esta va a reaccionar cuando la variable le hagan un next en otro componente
    this.pacienteService.pacienteCambio.subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.pacienteService.mensaje.subscribe(data => {
        this.snackBar.open(data, null, {
          duration: 2500
        })
      });
    });

    /** 
    this.pacienteService.listarPacientes().subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    */

   this.pacienteService.listarPacientesPageable(0, 10).subscribe(data => {
    let pacientes = JSON.parse(JSON.stringify(data)).content;
    this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
    this.dataSource = new MatTableDataSource(pacientes);
    this.dataSource.sort = this.sort;
  })




  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idPaciente: number) {
    this.pacienteService.eliminar(idPaciente).subscribe(data => {
      this.pacienteService.listarPacientes().subscribe(data => {
        this.lista = data;
        this.dataSource = new MatTableDataSource(this.lista);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });
    this.snackBar.open('Se eliminó correctamente', null, {
      duration: 2500
    })
  }

  mostrarMas(e: any){
    this.pacienteService.listarPacientesPageable(e.pageIndex, e.pageSize).subscribe(data => {
      let pacientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource= new MatTableDataSource(pacientes);
      
      this.dataSource.sort = this.sort;
      
    });
  }

}
