import { Component, OnInit, Inject } from '@angular/core';
import { Consulta } from '../../../../_model/consulta';
import { ConsultaListaExamen } from '../../../../_model/consulta-lista-examen';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ExamenService } from '../../../../_service/examen.service';

@Component({
  selector: 'app-dialogo-detalle',
  templateUrl: './dialogo-detalle.component.html',
  styleUrls: ['./dialogo-detalle.component.css']
})
export class DialogoDetalleComponent implements OnInit {

  consulta: Consulta;
  examenes: ConsultaListaExamen[];

  constructor(public dialogRef: MatDialogRef<DialogoDetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Consulta,
    private examenService: ExamenService) { }

  ngOnInit() {
    this.consulta = this.data;
    console.log(this.data);
    this.listarExamenes();
  }

  listarExamenes() {
    //this.examenService.listarExamenPorConsulta(this.consulta.idQuery).subscribe((data) => {
    //  this.examenes = data;
    //});
  }

  cancelar() {
    this.dialogRef.close();
  }

}
