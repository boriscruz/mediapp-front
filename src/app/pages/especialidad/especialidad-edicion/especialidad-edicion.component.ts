import { Component, OnInit } from '@angular/core';
import { Especialidad } from '../../../_model/especialidad';
import { FormGroup, FormControl } from '@angular/forms';
import { EspecialidadService } from '../../../_service/especialidad.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-especialidad-edicion',
  templateUrl: './especialidad-edicion.component.html',
  styleUrls: ['./especialidad-edicion.component.css']
})
export class EspecialidadEdicionComponent implements OnInit {

  
  id: number;
  especialidad: Especialidad = new Especialidad();
  form: FormGroup;
  edicion: boolean = false;

  constructor(
    private especialidadService: EspecialidadService, 
    private route: ActivatedRoute, 
    private router: Router) {
    this.form = new FormGroup({
      'idSpeciality': new FormControl(0),
      'name': new FormControl('')
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.especialidadService.listarEspecialidadPorId(this.id).subscribe(data => {
        this.especialidad = data;
        this.form = new FormGroup({
          'idSpeciality': new FormControl(this.especialidad.idSpeciality),
          'name': new FormControl(this.especialidad.name)
        });
      });
    }
  }

  operar() {
    this.especialidad = this.form.value;

    if (this.especialidad != null && this.especialidad.idSpeciality > 0) {
      this.especialidadService.modificar(this.especialidad).subscribe(data => {
        this.especialidadService.listarEspecialidades().subscribe(especialidad => {
          this.especialidadService.especialidadCambio.next(especialidad);
          this.especialidadService.mensaje.next("Se modificó");
        });
      });
    } else {
      this.especialidadService.registrar(this.especialidad).subscribe(data => {
        this.especialidadService.listarEspecialidades().subscribe(especialidad => {
          this.especialidadService.especialidadCambio.next(especialidad);
          this.especialidadService.mensaje.next("Se registró");
        });
      });
    }

    this.router.navigate(['especialidad']);
  }
}
