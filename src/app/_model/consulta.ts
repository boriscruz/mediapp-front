import { Paciente } from "./paciente";
import { Medico } from "./medico";
import { Especialidad } from "./especialidad";
import { DetalleConsulta } from "./detalle-consulta";
import { Examen } from "./examen";

export class Consulta{
    public idQuery: number;
    public patient: Paciente ;
    public date: Date;
    public doctor: Medico;
    public speciality: Especialidad;
    public detailQuery: DetalleConsulta[];
    public examList: Examen[];
}