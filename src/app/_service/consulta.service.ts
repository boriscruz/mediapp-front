import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Consulta } from '../_model/consulta';
import { URL_CONSULTAS, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FiltroConsulta } from '../_model/filtro.consulta';
import { ConsultaResumen } from '../_model/consulta-resumen';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  ConsultaCambio = new Subject<Consulta[]>();
  mensaje = new Subject<string>();
  url: string = URL_CONSULTAS;

  constructor(private http: HttpClient) { }

  listarConsultas(){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Consulta[]>(this.url, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  listarConsultaPorId(id: number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Consulta>(`${this.url}/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  registrar(consulta: Consulta){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(this.url, consulta, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  modificar(consulta: Consulta){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(this.url, consulta, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  eliminar(id: number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete(`${this.url}/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  buscar(filtroConsulta: FiltroConsulta) {    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post<Consulta[]>(`${this.url}/buscar`, filtroConsulta, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  listarResumen() {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ConsultaResumen[]>(`${this.url}/listarResumen`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  generarReporte() {    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get(`${this.url}/generarReporte`, {
      responseType: 'blob', // Representa un conjunto de bites, la salida de este servicio es un arreglo de bites
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  guardarArchivo(data: File) {    
    let formdata: FormData = new FormData();
    formdata.append('file', data);

    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    
    return this.http.post(`${this.url}/guardarArchivo`, formdata, {
      responseType: 'text',
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`)
    });
  }

  leerArchivo() {    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get(`${this.url}/leerArchivo/1`, {
      responseType: 'blob',
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }


}
