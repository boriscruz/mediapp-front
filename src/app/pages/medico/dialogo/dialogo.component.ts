import { Component, OnInit, Inject } from '@angular/core';
import { Medico } from '../../../_model/medico';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MedicoService } from '../../../_service/medico.service';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit {

  medico: Medico;

  constructor(public dialogRef: MatDialogRef<DialogoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Medico,
    private medicoService: MedicoService) {

  }

  ngOnInit() {
    this.medico = new Medico();
    this.medico.id = this.data.id;
    this.medico.name = this.data.name;
    this.medico.lastname = this.data.lastname;
    this.medico.cmp = this.data.cmp;
  }

  operar() {

    if (this.medico != null && this.medico.id > 0) {
      this.medicoService.modificar(this.medico).subscribe(data => {
        this.medicoService.listarMedicos().subscribe(medicos => {
          this.medicoService.medicosCambio.next(medicos);
          this.medicoService.mensaje.next("Se modifico");
        });
      });
    } else {
      this.medicoService.registrar(this.medico).subscribe(data => {
        this.medicoService.listarMedicos().subscribe(medicos => {
          this.medicoService.medicosCambio.next(medicos);
          this.medicoService.mensaje.next("Se registro");
        });
      });
    }
    this.dialogRef.close();
  }

  cancelar() {
    this.dialogRef.close();
  }
}
