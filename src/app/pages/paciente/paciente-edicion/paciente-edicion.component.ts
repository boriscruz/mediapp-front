import { Component, OnInit } from '@angular/core';
import { Paciente } from '../../../_model/paciente';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PacienteService } from '../../../_service/paciente.service';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-paciente-edicion',
  templateUrl: './paciente-edicion.component.html',
  styleUrls: ['./paciente-edicion.component.css']
})
export class PacienteEdicionComponent implements OnInit {

  id: number;
  paciente: Paciente = new Paciente();
  form: FormGroup;
  edicion: boolean = false;

  constructor(
    private pacienteService: PacienteService, 
    private route: ActivatedRoute, 
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'name': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'lastname': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'dni': new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      'address': new FormControl('', [Validators.minLength(3), Validators.maxLength(150)]),
      'phone': new FormControl('', [Validators.minLength(9), Validators.maxLength(9)])
    });
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.pacienteService.listarPacientePorId(this.id).subscribe(data => {
        this.paciente = data;
        console.log(data);
        this.form = new FormGroup({
          'id': new FormControl(this.paciente.id),
          'name': new FormControl(this.paciente.name, [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
          'lastname': new FormControl(this.paciente.lastname, [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
          'dni': new FormControl(this.paciente.dni, [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
          'address': new FormControl(this.paciente.address, [Validators.minLength(3), Validators.maxLength(150)]),
          'phone': new FormControl(this.paciente.phone, [Validators.minLength(9), Validators.maxLength(9)])
        });
      });
    }
  }



  operar() {
    this.paciente = this.form.value;
    console.log(this.paciente);
    if (this.edicion) {
      //update
      this.pacienteService.modificar(this.paciente).subscribe(data => {
        console.log(data);
        //if (data === 1) {
          this.pacienteService.listarPacientes().subscribe(pacientes => {
            // La variable reactiva toma la lista de pacientes que debe reaccionar
            // Con el next lo que hace es avisar a todos los que estan usando esta variable
            // que se está modificando el contenido de la variable
            this.pacienteService.pacienteCambio.next(pacientes);
            this.pacienteService.mensaje.next('Se modificó');
          });
        //} else {
          //this.pacienteService.mensaje.next('No se modificó');
        //}
      });
    } else {
      //insert
      this.pacienteService.registrar(this.paciente).subscribe(data => {
        console.log(data);
        //if (data === 1) {
          this.pacienteService.listarPacientes().subscribe(pacientes => {
            // La variable reactiva toma la lista de pacientes que debe reaccionar
            this.pacienteService.pacienteCambio.next(pacientes);
            this.pacienteService.mensaje.next('Se registró');
          });
        //} else {
          //this.pacienteService.mensaje.next('No se registró');
        //}
      });
    }

    this.router.navigate(['paciente'])
  }

}
