// let: definición de una variable en contexto local
// const: denición de una constante
// ******************** SERVICIOS *****************************
export const HOST = 'http://localhost:8080';

export const URL_PACIENTES = `${HOST}/patients`;

export const URL_MEDICOS = `${HOST}/doctors`;

export const URL_EXAMENES = `${HOST}/examinations`;

export const URL_ESPECIALIDADES = `${HOST}/specialities`;

export const URL_CONSULTAS = `${HOST}/queries`;

export const URL_MENUS = `${HOST}/menus`;

// ******************** VARIABLES *****************************
export const REINTENTOS = 3;

export const TOKEN_AUTH_USERNAME = 'mitomediaapp';
export const TOKEN_AUTH_PASSWORD = 'mito89codex';
export const TOKEN_NAME = 'access_token';