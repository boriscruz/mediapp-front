export class Medico{
    public id: number;
    public name: string ;
    public lastname: string;
    public dni: string;
    public address: string;
    public phone: string;
    public cmp: string;
}